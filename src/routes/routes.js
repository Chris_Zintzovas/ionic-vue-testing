let Home = () => import('@/components/HelloWorld.vue')

const routes =  [
	{
		name: 'Home',
		path: '/',
		component: Home
	}
]

export default routes;